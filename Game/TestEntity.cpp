#include <iostream>

#include "Game/EntFactory.h"
#include "Game/TestEntity.h"
#include "Common/Glue.h"

namespace CNWoofer
{
	namespace Game
	{
		void TestEntity::Spawn( void )
		{
			std::cout << "Test entity spawned." << std::endl;

			pSprite = gpGlue->CreateSprite( "random", &iXPos, &iYPos );
			pSprite->SetAnimation( "run" );
		}

		void TestEntity::Update( void )
		{
			iXPos = rand() % 300;
			iYPos = rand() % 300;

			fNextUpdate = gpGlue->GetTime() + 1;
		}

		void TestEntity::Draw( void )
		{
//			gpGlue->DrawString( "Test Entity!", iXPos, iYPos );			
		}

		void TestEntity::Death( void )
		{

		}

		DECLARE_ENTITY( TestEnt, TestEntity );
	}
}