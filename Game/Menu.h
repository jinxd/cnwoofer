#ifndef MENU_H
#define MENU_H

/*

MENU.H:		Menu interface.

*/

#include <vector>

namespace CNWoofer
{
	namespace Game
	{

		class Option
		{
		public:

			int iPosition;
			
			virtual void Draw( int x, int y, bool selected ) = 0;
			virtual void Selected( void ) = 0;

		};

		class Menu
		{
		public:

			int iActiveOption;
			int iNumOptions;

			std::vector<Option*> mOptions;

			Menu( void ){}
			~Menu( void ){}

			virtual void Activate( void ) = 0;
			virtual void Update( void ) = 0;
			virtual void Deactivate( void ) = 0;

		};

		class Quit : public Option
		{
		public:
			
			Quit( int pos )
			{
				iPosition = pos;
			}

			virtual void Draw( int x, int y, bool selected );
			virtual void Selected( void );

		};

		class NewGame : public Option
		{
		public:

			NewGame( int pos )
			{
				iPosition = pos;
			}

			virtual void Draw( int x, int y, bool selected );
			virtual void Selected( void );

		};

		class MainMenu : public Menu
		{
		public:

			MainMenu( void );
			~MainMenu( void );

			virtual void Activate( void );
			virtual void Update( void );
			virtual void Deactivate( void );


		};
	}
}

#endif