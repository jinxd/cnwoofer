#include "Game/Game.h"
#include "Game/EntFactory.h"

#include "Game/Menu.h"

#include "Game/Player.h"

#include <iostream>

namespace CNWoofer
{
	namespace Game
	{
		Common::Glue* gpGlue;
		MainMenu* gpMainMenu;
		Player* gpPlayer;

		void GameInit( Common::Glue* glueptr )
		{
			std::cout << "Game DLL loaded." << std::endl;

			gpGlue = glueptr;

			gpEntFactory = new EntFactory;
			gpMainMenu = new MainMenu;
			gpPlayer = (Player*)gpEntFactory->CreateEntity( "player" );
		}

		void GameLoop( void )
		{
			if( gpGlue->GetKeyState( "menu" ) == Common::KEY_PRESSED )
			{
				if( gpGlue->GetState() == Common::ENGINE_GAME )
				{
					gpGlue->SetState( Common::ENGINE_MENU );
					
					gpMainMenu->Activate();
				}
				else if( gpGlue->GetState() == Common::ENGINE_MENU )
				{
					gpGlue->SetState( Common::ENGINE_GAME );

					gpMainMenu->Deactivate();
				}
			}

			if( gpGlue->GetState() == Common::ENGINE_MENU )
			{
				gpMainMenu->Update();
			}

			if( gpGlue->GetState() == Common::ENGINE_GAME )
			{
				if( gpGlue->GetKeyState( "space" ) == Common::KEY_PRESSED )
				{
					gpEntFactory->CreateEntity( "TestEnt" );
				}
			}
		}
	}
}