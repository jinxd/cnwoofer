#ifndef ENTFACTORY_H
#define ENTFACTORY_H

#include <map>
#include <string>

#include "Common/Entity.h"

namespace CNWoofer
{
	namespace Game
	{
		class EntFactory
		{
		public:

			Common::Entity* CreateEntity( std::string entname );

		};

		class EntMaker
		{
		public:

			EntMaker( std::string classname );
			virtual Common::Entity* CreateEntity() = 0; 

		};

		extern std::map<std::string, EntMaker*> gEntMakers;
		extern EntFactory* gpEntFactory;

		#define DECLARE_ENTITY( entName, gameClass ) \
				class gameClass##Maker : public EntMaker { \
				public: \
					gameClass##Maker( std::string className ) : \
					EntMaker( className ) {} \
					Common::Entity *CreateEntity() { return new gameClass##(); } }; \
					gameClass##Maker g##gameClass##Maker( #entName );
	}
}

#endif
