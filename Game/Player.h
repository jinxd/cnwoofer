#ifndef PLAYER_H
#define PLAYER_H

#include "Common/Entity.h"
#include "Common/Sprite.h"

namespace CNWoofer
{
	namespace Game
	{
		class Player : public Common::Entity
		{
			
			Common::Sprite* pSprite;

		public:

			Player( void ){}
			~Player( void ){}

			virtual void Spawn( void );
			virtual void Update( void );
			virtual void Death( void );
			virtual void Draw( void );
		};
	}
}
#endif