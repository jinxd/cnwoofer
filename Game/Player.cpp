#include "Game/Player.h"
#include "Game/EntFactory.h"

#include "Common/Glue.h"

namespace CNWoofer
{
	namespace Game
	{
		void Player::Spawn( void )
		{
			pSprite = gpGlue->CreateSprite( "test", &iXPos, &iYPos );
		}

		void Player::Update( void )
		{
			if( gpGlue->GetKeyState( "left" ) )
			{
				pSprite->SetAnimation( "run" );
				iXPos--;
			}
			else if( gpGlue->GetKeyState( "right" ) )
			{
				pSprite->SetAnimation( "run" );
				iXPos++;
			}
			else if( gpGlue->GetKeyState( "up" ) )
			{
				pSprite->SetAnimation( "jump" );
			}
			else
			{
				pSprite->SetAnimation( "idle" );
			}
		}

		void Player::Draw( void )
		{

		}

		void Player::Death( void )
		{

		}

		DECLARE_ENTITY( player, Player );
	}
}