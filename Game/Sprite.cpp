#include "Common/Sprite.h"

#include <string>
#include <iostream>

namespace CNWoofer
{
	namespace Common
	{
		Sprite::Sprite( void )
		{

		}

		Sprite::~Sprite( void )
		{

		}

		Sprite& Sprite::operator=(const Sprite& copy)
		{
			if( this != &copy )
			{
				State = copy.State;
				SpriteName = copy.SpriteName;

				Animations = copy.Animations;
				CurAnim = copy.CurAnim;
			}

			return *this;
		}

		void Sprite::Update( float time )
		{
			if( CurAnim->LastFrameTime + 1.0 / CurAnim->FPS <= time )
			{
				if( CurAnim->CurFrame == CurAnim->NumFrames - 1 )
				{
					CurAnim->CurFrame = 0;
				}
				else
				{
					CurAnim->CurFrame++;
				}

				CurAnim->LastFrameTime = time;
			}
		}

		void Sprite::SetAnimation( std::string name )
		{
			if( Animations[name] )
			{
				CurAnim = Animations[name];
			}
		}

		void Sprite::SetState( int state )
		{
			State = state;
		}
	}

}