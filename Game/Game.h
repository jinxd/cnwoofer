#ifndef GAME_H
#define GAME_H

/*

GAME.H:		Entry point for game code DLL.

*/

#ifdef GAME_EXPORTS
#define GAMEDLL __declspec( dllexport )
#else
#define GAMEDLL __declspec( dllimport )
#endif

#include "Common/Glue.h"

namespace CNWoofer
{
	namespace Game
	{
		extern Common::Glue* gpGlue;

		void GAMEDLL GameInit( Common::Glue* glueptr );
		void GAMEDLL GameLoop( void );
	}
}

#endif