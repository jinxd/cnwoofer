#include <iostream>

#include "Game/EntFactory.h"
#include "Common/Glue.h"

namespace CNWoofer
{
	namespace Game
	{
		std::map<std::string, EntMaker*> gEntMakers;

		EntMaker::EntMaker( std::string classname )
		{
			gEntMakers[classname] = this;
		}

		Common::Entity* EntFactory::CreateEntity( std::string entname )
		{
			Common::Entity* e = NULL;
			EntMaker* m = NULL;

			m = gEntMakers[entname];

			if( m )
			{
				e = m->CreateEntity();
			}
			if( !e )
			{
				std::cout << "Unable to create entity: " << entname << std::endl;
				return NULL;
			}

			gpGlue->AddEntity( e );

			std::cout << "Added entity: " << entname << " UID: " << e->iUID << std::endl;

			return e;
		}

		EntFactory* gpEntFactory;
	}
}