#ifndef TESTENTITY_H
#define TESTENTITY_H

#include "Common/Entity.h"
#include "Common/Sprite.h"

namespace CNWoofer
{
	namespace Game
	{
		class TestEntity : public Common::Entity
		{
			Common::Sprite* pSprite;

		public:

			TestEntity( void ) {}
			~TestEntity( void ) {}

			virtual void Spawn( void );
			virtual void Update( void );
			virtual void Death( void );
			virtual void Draw( void );
		};
	}
}
#endif