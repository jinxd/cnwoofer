#include "Common/Glue.h"
#include "Game/Menu.h"

namespace CNWoofer
{
	namespace Game
	{
		void Quit::Draw( int x, int y, bool selected )
		{
			gpGlue->DrawString( "Quit", x, y );
		}

		void Quit::Selected( void )
		{
			gpGlue->SetState( Common::ENGINE_SHUTDOWN );
		}

		void NewGame::Draw( int x, int y, bool selected )
		{
			gpGlue->DrawString( "New Game", x, y );
		}

		void NewGame::Selected( void )
		{
			gpGlue->SetState( Common::ENGINE_GAME ); 
		}

		MainMenu::MainMenu( void )
		{
			iActiveOption = 0;
			iNumOptions = 0;

			mOptions.push_back( new NewGame( iNumOptions++ ) );
			mOptions.push_back( new Quit( iNumOptions++ ) );
		}

		MainMenu::~MainMenu( void )
		{

		}

		void MainMenu::Activate( void )
		{
			
		}

		void MainMenu::Update( void )
		{
			gpGlue->DrawImage( "data/background.png", 0, 0 );

			if( gpGlue->GetKeyState( "up" ) == Common::KEY_PRESSED )
			{
				if( iActiveOption > 0 )
					iActiveOption--;
			}
			else if( gpGlue->GetKeyState( "down" ) == Common::KEY_PRESSED )
			{
				if( iActiveOption < mOptions.size() - 1 )
					iActiveOption++;
			}
			else if( gpGlue->GetKeyState( "enter" ) == Common::KEY_PRESSED )
			{
				mOptions[iActiveOption]->Selected();
			}

			std::vector<Option*>::iterator i = mOptions.begin();

			while( i != mOptions.end() )
			{
				bool selected = false;

				if( iActiveOption == (*i)->iPosition )
					selected = true;

				(*i)->Draw( 25, 15 + ((*i)->iPosition * 30), selected );

				i++;
			}

			gpGlue->DrawString( ">", 5, 15 + iActiveOption * 30 );
		}

		void MainMenu::Deactivate( void )
		{

		}
	}
}
