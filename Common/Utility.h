#ifndef UTILITY_H
#define UTILITY_H

/*

UTILITY.H:	Contains utility functions.

*/

#include <SFML/System/Vector2.hpp>

namespace CNWoofer
{
	namespace Common
	{
		inline bool AABB( sf::Vector2f aa, sf::Vector2f ab, sf::Vector2f ba, sf::Vector2f bb )
		{
			/* Top left corner */
			if( aa.x > ba.x && aa.x < bb.x )
			{
				if( aa.y > ba.y && aa.y < bb.y )
				{
					return true;
				}
			}

			/* Bottom right corner */
			if( ab.x > ba.x && ab.x < bb.x )
			{
				if( ab.y > ba.y && ab.y < bb.y )
				{
					return true;
				}
			}

			float width = ab.x - aa.x;
			float height = ab.y - aa.y;

			/* Bottom left corner */
			if( aa.x > ba.x && aa.x < bb.x )
			{
				if( aa.y + height > ba.y && aa.y + height < bb.y )
				{
					return true;
				}
			}

			/* Top right corner */
			if( aa.x + width > ba.x && aa.x + width < bb.x )
			{
				if( ab.y > ba.y && ab.y < bb.y )
				{
					return true;
				}
			}

			return false;
		}
	}
}

#endif