#ifndef ENTITY_H
#define ENTITY_H

/*

ENTITY.H:	Common entity base class shared between game code
			and the engine.

	@Spawn(): Called on the creation of the entity.

	@Update(): Called every frame.

	@Death(): Called when entity status has been set to 'delete'.
			  Entity is deleted from memory after this has been called.

*/

namespace CNWoofer
{
	namespace Common
	{
		class Entity
		{
		public:

			Entity( void )
			{
				iHealth = 1;
				fNextUpdate = 0;
				iXPos = 0;
				iYPos = 0;
				iUID = -999;
			}
			~Entity( void )
			{

			}

			int iUID, iType, iHealth, iXPos, iYPos;
			float fNextUpdate;

			virtual void Spawn( void ) = 0;
			virtual void Update( void ) = 0;
			virtual void Death( void ) = 0;
			virtual void Draw( void ) = 0;
		};
	}
}

#endif