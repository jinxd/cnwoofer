#ifndef SPRITE_H
#define SPRITE_H

#ifdef GAME
#define DllExport   __declspec( dllexport )
#else
#define DllExport   __declspec( dllimport )
#endif 

#include <string>
#include <map>
#include <vector>

#include <SFML/Graphics.hpp>

namespace CNWoofer
{
	namespace Common
	{
		enum
		{
			SPRITE_PAUSE = 0,
			SPRITE_PLAY,
			SPRITE_REMOVE
		};

		class Animation
		{

		public:

			float LastFrameTime;
			int CurFrame;
			int NumFrames;
			int FPS;

			int FrameWidth;
			int FrameHeight;

			std::vector<sf::Texture*> Frames;
		};

		class DllExport Sprite
		{

		public:

			int State;

			int *PosX, *PosY;

			std::string SpriteName;

			std::map<std::string, Animation*> Animations;
			Animation* CurAnim;

			Sprite( void );
			~Sprite( void );

			Sprite& operator=(const Sprite& copy);

			void Update( float time );
			void SetAnimation( std::string name );
			void SetState( int state );

		};
	}
}

#endif