#ifndef GLUE_H
#define GLUE_H

/*

GLUE.H:		Boundary layer between engine and game code.
			Provides a nice, neat interface to engine functions.

*/

#include <SFML/System/Vector2.hpp>
#include <string>

#include "Common/Keys.h"
#include "Common/Entity.h"
#include "Common/Sprite.h"

namespace CNWoofer
{
	namespace Common
	{
		enum
		{
			ENGINE_SHUTDOWN = 0,
			ENGINE_GAME,
			ENGINE_MENU
		};

		class Glue
		{
		public:

			void (*AddEntity)( Entity* entity );
			void (*LoadMap)( std::string filename );
			void (*DrawString)( std::string str, int x, int y );
			void (*DrawImage)( std::string img, int x, int y );
			void (*BindKey)( std::string name, int key );
			int (*GetKeyState)( std::string name );
			int (*GetState)( void );
			void (*SetState)( int state );
			float (*GetTime)( void );
			Sprite* (*CreateSprite)( std::string name, int* x, int* y ); 

		};

	}

#ifdef GAME
	namespace Game
	{
		extern Common::Glue* gpGlue;
	}
#endif

}

#endif GLUE_H