#ifndef ENGINE_H
#define ENGINE_H

/*

ENGINE.H:	Engine class for binding together all modules and baseline
			functions. Input events and entity management are handled
			in this class.

*/

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <map>
#include <string>

#include "Engine/ResMgr.h"
#include "Engine/Render.h"
#include "Engine/Sound.h"
#include "Engine/Map.h"
#include "Engine/SpriteMgr.h"

#include "Common/Glue.h"
#include "Common/Keys.h"
#include "Common/Entity.h"

namespace CNWoofer
{
	namespace Engine
	{
		class Button
		{
		public:

			std::string sName;
			int iKeyCode;
			int iState;
		};

		class Core
		{
			sf::RenderWindow* mWindow;

			std::map<std::string, Button*> mSButtons;
			std::map<int, Button*> mIButtons;
			std::map<int, Common::Entity*> mEntities;

			int iFPS, iFPSCounter;
			int iDelta;

			sf::Clock mFPSClock;
			sf::Clock mClock;

			int iLastFrameTime;

			int iEntityID;

			void CalcFPS( void );
			void UpdateKeys( void );

		public:

			Common::Glue* pGlue;

			SpriteMgr* mSpriteMgr;
			ResourceMgr* mResMgr;
			Render* mRender;
			Sound* mSound;

			Map* pCurrentMap;

			int iEngineState;

			Core( void );
			~Core( void );

			int Initialize( void );
			void Frame( void );
			int Shutdown( void );

			void Events( void );

			void KeyPressed( int key );
			void KeyReleased( int key );
			int GetKeyState( std::string name );
			void BindKey( std::string name, int key );

			int GetFPS( void );
			int GetDelta( void );
			float GetTime( void );

			int GetState( void );
			void SetState( int state );

			void AddEntity( Common::Entity* );
			void UpdateEntities( void );

		};
	}
}

#endif 