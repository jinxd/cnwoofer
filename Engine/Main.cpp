#include "Engine.h"
#include "Game/Game.h"

namespace CNWoofer
{
	namespace Engine
	{
		Core* pEngine;
	}
	namespace Common
	{
		void InitGlue( void );
	}
}

using namespace CNWoofer;

int main( int argc, char* argv[] )
{
	Engine::pEngine = new Engine::Core;

	if( Engine::pEngine->Initialize() < 0 )
	{
		delete Engine::pEngine;
		return -1;
	}

	Common::InitGlue();

	Game::GameInit( Engine::pEngine->pGlue );

	while( Engine::pEngine->GetState() )
	{		
		Engine::pEngine->mRender->PreRender();

		Engine::pEngine->Frame();
		Game::GameLoop();

		Engine::pEngine->mRender->PostRender();
	}

	Engine::pEngine->Shutdown();

	delete Engine::pEngine;

	return 0;
}