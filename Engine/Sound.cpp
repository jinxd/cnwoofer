#include "Sound.h"

namespace CNWoofer
{
	namespace Engine
	{
		Sound::Sound( void )
		{

		}

		Sound::~Sound( void )
		{
			std::list<sf::Sound*>::iterator i = mSounds.begin();

			while( i != mSounds.end() )
			{
				delete (*i);

				i = mSounds.erase( i );
			}			
		}
	
		void Sound::PlaySound( sf::SoundBuffer* buffer )
		{
			sf::Sound* sound = new sf::Sound;

			sound->setBuffer( *buffer );
			sound->play();
		
			mSounds.push_back( sound );
		}

		void Sound::Update( void )
		{
			std::list<sf::Sound*>::iterator i = mSounds.begin();

			while( i != mSounds.end() )
			{
				if( (*i)->getStatus() == sf::SoundSource::Stopped )
				{
					delete (*i);

					i = mSounds.erase( i );
				}
				else
				{
					i++;
				}
			}
		}

		void Sound::SetVolume( float volume )
		{
			if( volume < 0 )
			{
				volume = 0;
			}
			else if( volume > 100 )
			{
				volume = 100;
			}

			sf::Listener::setGlobalVolume( volume );
		}
	}
}