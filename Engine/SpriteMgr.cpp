#include "Common/Sprite.h"
#include "SpriteMgr.h"
#include "Render.h"

#include <string>
#include <iostream>

namespace CNWoofer
{
	namespace Engine
	{
		SpriteMgr::SpriteMgr( ResourceMgr* resmgr, Render* render )
		{
			pResMgr = resmgr;
			pRender = render;
		}

		SpriteMgr::~SpriteMgr( void )
		{

		}

		void SpriteMgr::UpdateSprites( float time )
		{
			std::vector<Common::Sprite*>::iterator i = mSprites.begin( );

			while( i < mSprites.end( ) )
			{
				if( (*i)->State == Common::SPRITE_PLAY )
				{
					(*i)->Update( time );
				}
				else if( (*i)->State == Common::SPRITE_REMOVE )
				{
					delete (*i);

					mSprites.erase( i );
				}
				i++;
			}
		}

		void SpriteMgr::DrawSprites( void )
		{
			std::vector<Common::Sprite*>::iterator i = mSprites.begin( );
			sf::Vector2f vec;

			while( i < mSprites.end( ) )
			{
				vec.x = (*(*i)->PosX);
				vec.y = (*(*i)->PosY);

				pRender->DrawQuad( (*i)->CurAnim->Frames[(*i)->CurAnim->CurFrame], vec );

				i++;
			}
		}

		Common::Sprite* SpriteMgr::CreateSprite( std::string name, int* x, int* y )
		{
			Common::Sprite* spr = NULL;
			Common::Sprite* temp = NULL;

			spr = (Common::Sprite*)pResMgr->LoadResource( "data/sprites/" + name + ".spr" );

			if( spr )
			{
				spr->PosX = x;
				spr->PosY = y;

				mSprites.push_back( spr );
			}

			return spr;
		}
	}
}