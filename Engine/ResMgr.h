#ifndef RESMGR_H
#define RESMGR_H

/*

RESMGR.H:	Resource manager for loading all types of data.
			Data is stored in a std::map with a name referencing
			a void pointer to prevent loading the same resource
			multiple times and for quick look up of already loaded
			resources.

*/

#include <string>
#include <map>

#include "Map.h"

namespace CNWoofer
{
	namespace Engine
	{
		enum
		{
			RESOURCE_TEXTURE = 0,
			RESOURCE_IMAGE,
			RESOURCE_SOUND,
			RESOURCE_MAP,
			RESOURCE_FONT,
			RESOURCE_SPRITE
		};

		class Resource
		{
		public:

			int ResType;
			void* ResPtr;
		};

		class ResourceMgr
		{
			std::map<std::string, Resource*> mResources;

			std::vector<Tile*> LoadLayer( Map* map, std::stringstream& ss );

			void* LoadTexture( std::string filename );
			void* LoadImage( std::string filename );
			void* LoadTMX( std::string filename );
			void* LoadFont( std::string filename );
			void* LoadSound( std::string filename );
			void* LoadSprite( std::string filename );

		public:

			ResourceMgr( void );
			~ResourceMgr( void );

			void UnloadResources( int type = -1 );

			void* LoadResource( std::string filename, int options = -1 );
		};
	}
}

#endif RESMGR_H