#ifndef MAP_H
#define MAP_H

#include <SFML/Graphics.hpp>
#include <vector>

namespace CNWoofer
{
	namespace Engine
	{
		class Tile
		{
		public:

			int GID;
			int X, Y;
		};

		class CollisionRect
		{
			int X, Y;
			int Width, Height;
		};

		class Map
		{
		public:

			int TilesX, TilesY;
			int TileSize;

			std::string Name;

			std::map<int, sf::Texture*> TileSet;
			std::vector<Tile*> Foreground;
			std::vector<Tile*> Background;
			
		};
	}
}

#endif MAP_H