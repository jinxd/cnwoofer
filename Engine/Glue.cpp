#include "Common/Glue.h"
#include "Engine/Engine.h"
#include "Engine/Map.h"

namespace CNWoofer
{
	namespace Engine
	{
		extern Core* pEngine;
	}

	namespace Common
	{
		void AddEntity( Entity* entity )
		{
			Engine::pEngine->AddEntity( entity );
		}

		void LoadMap( std::string filename )
		{
			Engine::pEngine->pCurrentMap = (Engine::Map*)Engine::pEngine->mResMgr->LoadResource( filename );
		}

		void DrawString( std::string str, int x, int y )
		{
			Engine::pEngine->mRender->DrawString( str, sf::Vector2f( (float)x, (float)y ) );
		}

		void BindKey( std::string name, int key )
		{
			Engine::pEngine->BindKey( name, key );
		}

		int GetKeyState( std::string name )
		{
			return Engine::pEngine->GetKeyState( name );
		}
		
		int GetState( void )
		{
			return Engine::pEngine->GetState();
		}

		void SetState( int state )
		{
			Engine::pEngine->SetState( state );
		}

		void DrawImage( std::string img, int x, int y )
		{
			Engine::pEngine->mRender->DrawQuad( 
				(sf::Texture*)Engine::pEngine->mResMgr->LoadResource( img ), 
				sf::Vector2f( (float)x, (float)y ) );
		}

		float GetTime( void )
		{
			return Engine::pEngine->GetTime();
		}

		Sprite* CreateSprite( std::string name, int* x, int* y )
		{
			return Engine::pEngine->mSpriteMgr->CreateSprite( name, x, y );
		}

		void InitGlue( void )
		{
			Engine::pEngine->pGlue->LoadMap = &LoadMap;
			Engine::pEngine->pGlue->AddEntity = &AddEntity;
			Engine::pEngine->pGlue->DrawString = &DrawString;
			Engine::pEngine->pGlue->BindKey = &BindKey;			
			Engine::pEngine->pGlue->GetKeyState = &GetKeyState;
			Engine::pEngine->pGlue->GetState = &GetState;
			Engine::pEngine->pGlue->SetState = &SetState;
			Engine::pEngine->pGlue->DrawImage = &DrawImage;
			Engine::pEngine->pGlue->GetTime = &GetTime;
			Engine::pEngine->pGlue->CreateSprite = &CreateSprite;
		}
	}
}