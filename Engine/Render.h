#ifndef RENDER_H
#define RENDER_H

/*

RENDER.H:	Contains rendering procedures

*/

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include "Engine/Map.h"

namespace CNWoofer
{
	namespace Engine
	{	
		class Render
		{
			
			sf::RenderWindow* mWindow;

			sf::Text mText;
			sf::View mView;

			std::map<std::string, sf::Shader*> mShaders;

			std::string mCurrentShader;

			sf::RenderStates mRenderState;

			sf::Vector2f ScreenTL, ScreenBR;

		public:

			Render( void );
			~Render( void );

			void SetWindow( sf::RenderWindow* window );
			void SetFont( sf::Font* font );

			void EnableShader( std::string shader );
			void DisableShader();

			sf::Shader* LoadShader( std::string name, std::string vert, std::string frag );

			void MoveView( float x, float y );

			void PreRender( void );
			void PostRender( void );

			void DrawString( std::string str, sf::Vector2f pos );
			void DrawQuad( sf::Texture* tex, sf::Vector2f pos, float scale = 1.0f );
			void DrawMap( Map* map );

			bool IsOnScreen( sf::Vector2f tl, sf::Vector2f br );
			void GenScreenVecs( void );

		};
	}
}

#endif