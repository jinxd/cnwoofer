#ifndef SPRITEMGR_H
#define SPRITEMGR_H

#include <vector>
#include <map>

#include "common/sprite.h"
#include "ResMgr.h"
#include "Render.h"

namespace CNWoofer
{
	namespace Engine
	{
		class SpriteMgr
		{
			std::vector<Common::Sprite*> mSprites;

			ResourceMgr* pResMgr;
			Render* pRender;

		public:

			SpriteMgr( ResourceMgr* resmgr, Render* render );
			~SpriteMgr( void );

			void UpdateSprites( float time );
			void DrawSprites( void );
			Common::Sprite* CreateSprite( std::string name, int* x, int* y );
		};
	}
}

#endif