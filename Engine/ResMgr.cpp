#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <tinyxml.h>

#include "Engine/ResMgr.h"
#include "Engine/Map.h"

#include "Common/Sprite.h"

namespace CNWoofer
{
	namespace Engine
	{
		ResourceMgr::ResourceMgr( void )
		{

		}

		ResourceMgr::~ResourceMgr( void )
		{

		}
	
		void ResourceMgr::UnloadResources( int type )
		{
			std::map<std::string, Resource*>::iterator i = mResources.begin();
			
			if( type < 0 )
			{
				while( i != mResources.end() )
				{
					delete i->second->ResPtr;

					mResources.erase( i );

					i++;
				}
			}
			else
			{
				while( i != mResources.end() )
				{
					if( i->second->ResType == type )
					{
						delete i->second->ResPtr;

						mResources.erase( i );
					}

					i++;
				}
			}
		}

		void* ResourceMgr::LoadResource( std::string filename, int options )
		{ 
			Resource* res;
			std::string ext = filename.substr( filename.rfind( "." ) + 1 );;

			if( mResources[filename] != NULL && ext != "spr" )
			{
				return mResources[filename]->ResPtr;
			}
			else
			{
				if( ext == "bmp" || ext == "png" )
				{
					res = new Resource;
					if( options == RESOURCE_IMAGE )
					{
						res->ResPtr = LoadImage( filename );
						res->ResType = RESOURCE_IMAGE;
					}
					else
					{
						res->ResPtr = LoadTexture( filename );
						res->ResType = RESOURCE_TEXTURE;
					}
				}
				else if( ext == "ttf" )
				{
					res = new Resource;
					res->ResPtr = LoadFont( filename );
					res->ResType = RESOURCE_FONT;
				}
				else if (ext == "tmx")
				{
					res = new Resource;
					res->ResPtr = LoadTMX( filename );
					res->ResType = RESOURCE_MAP;
				}
				else if( ext == "wav" )
				{
					res = new Resource;
					res->ResPtr = LoadSound( filename );
					res->ResType = RESOURCE_SOUND;
				}
				else if( ext == "spr" )
				{
					res = new Resource;
					res->ResPtr = LoadSprite( filename );
					res->ResType = RESOURCE_SPRITE;
				}
			}

			if( !res->ResPtr )
			{
				std::cout << "Failed to load resource: " << filename << std::endl;

				delete res;
				return NULL;
			}

			mResources[filename] = res;

			std::cout << "Loaded resource: " << filename << std::endl;

			return mResources[filename]->ResPtr;
		}
		
		void* ResourceMgr::LoadTexture( std::string filename )
		{
			sf::Texture* tex = new sf::Texture;

			tex->loadFromFile( filename );		

			if( !tex )
			{
				delete tex;

				return NULL;
			}

			tex->setSmooth( false );

			return (void*)tex;
		}

		void* ResourceMgr::LoadImage( std::string filename )
		{
			sf::Image* img = new sf::Image;

			if( !img->loadFromFile( filename ) )
			{
				delete img;
				return NULL;
			}

			return (void*)img;
		}

		void* ResourceMgr::LoadFont( std::string filename )
		{
			sf::Font* font = new sf::Font;

			font->loadFromFile( filename );

			if( !font )
			{
				delete font;
				
				return NULL;
			}

			return (void*)font;
		}

		void* ResourceMgr::LoadTMX( std::string filename )
		{
			TiXmlDocument tmx;
			TiXmlElement *root, *temp;
			Map* map = NULL;

			if( tmx.LoadFile( filename ) )
			{
				root = tmx.FirstChildElement();
			
				if (!root)
					return NULL;

				map = new Map;

				root->Attribute( "width", &map->TilesX );
				root->Attribute( "height", &map->TilesY );
				root->Attribute( "tilewidth", &map->TileSize );

				for( temp = root->FirstChildElement(); temp != NULL; temp = temp->NextSiblingElement() )
				{
					std::string ename = temp->Value();

					if( ename == "properties" )
					{
						std::cout << "Loading Properties..." << std::endl;

						for( TiXmlElement* props = temp->FirstChildElement(); props != NULL; props = props->NextSiblingElement() )
						{
							if( !strcmp( props->Attribute( "name" ), "MapName" ) )
							{
								map->Name = props->Attribute( "value" );
								std::cout << "Map name is: " << map->Name << std::endl;
							}
						}
					}

					if( ename == "tileset" )
					{
						int gid, firstgid, tilecount, columns, tilewidth, tileheight;
						std::string imgfile;

						std::cout << "Loading Tileset..." << std::endl;

						temp->Attribute( "firstgid", &firstgid );
						temp->Attribute( "tilecount", &tilecount );
						temp->Attribute( "columns", &columns );
						temp->Attribute( "tilewidth", &tilewidth );
						temp->Attribute( "tileheight", &tileheight );

						gid = firstgid;

						TiXmlElement* image = temp->FirstChildElement();

						imgfile = image->Attribute( "source" );

						sf::Image* img = new sf::Image;

						img->loadFromFile( "data/" + imgfile );

						sf::Rect<int> bounds;

						int i = 0, x = 0, y = 0;
						while( i < tilecount )
						{
							if( x == columns )
							{
								x = 0;
								y++;
							}

							bounds.left = x * tilewidth;
							bounds.top = y * tileheight;
							bounds.width = tilewidth;
							bounds.height = tileheight;

							map->TileSet[gid] = new sf::Texture;
							map->TileSet[gid]->loadFromImage( *img, bounds );
							
							x++;
							gid++;
							i++;
						}
						
						delete img;
					}

					if( ename == "objectgroup" )
					{
						std::cout << "Loading Object Group..." << std::endl;

					}

					if( ename == "layer" )
					{
						std::cout << "Loading Tile Layer..." << std::endl;

						if( !strcmp( temp->Attribute( "name" ), "foreground" ) )
						{
							std::string tiledata = temp->FirstChildElement()->GetText();
							std::stringstream ss( tiledata );

							map->Foreground = LoadLayer( map, ss );

						}
		
						else if( !strcmp( temp->Attribute( "name" ), "background" ) )
						{
							std::string tiledata = temp->FirstChildElement( )->GetText( );
							std::stringstream ss( tiledata );

							map->Background = LoadLayer( map, ss );
						}
					}
				}
			}

			return map;
		}

		std::vector<Tile*> ResourceMgr::LoadLayer( Map* map, std::stringstream& ss )
		{
			std::vector<Tile*> tiles;
			Tile* t;

			int i = 0, x = 0, y = 0;

			while( ss >> i )
			{
				if( x == map->TilesX )
				{
					x = 0;
					y++;
				}

				if( i != 0 )
				{
					t = new Tile;

					t->X = x * map->TileSize;
					t->Y = y * map->TileSize;
					t->GID = i;

					tiles.push_back( t );
				}

				if( ss.peek( ) == ',' )
					ss.ignore( );

				x++;
			}

			return tiles;
		}

		void* ResourceMgr::LoadSound( std::string filename )
		{
			sf::SoundBuffer* snd = new sf::SoundBuffer;

			snd->loadFromFile( filename );

			if( !snd )
			{
				delete snd;

				return NULL;
			}

			return (void*)snd;
		}

		void* ResourceMgr::LoadSprite( std::string filename )
		{
			Common::Sprite* spr = NULL;
			TiXmlDocument sprxml;
			TiXmlElement* root;
			TiXmlElement* temp;

			std::string directory = filename.substr( 0, filename.rfind( '/' ) + 1 );

			if( sprxml.LoadFile( filename ) )
			{
				root = sprxml.FirstChildElement( );

				if( !root )
					return NULL;

				spr = new Common::Sprite;

				spr->SpriteName = root->Attribute( "name" );

				for( temp = root->FirstChildElement( ); temp != NULL; temp = temp->NextSiblingElement( ) )
				{
					std::string ename = temp->Value( );

					if( ename == "animation" )
					{
						Common::Animation* anim = new Common::Animation;
						int fwidth, fheight, twidth, theight, frames;

						std::string animname = temp->Attribute( "name" );
						std::string animfile = directory + std::string( temp->Attribute( "filename" ) );

						temp->Attribute( "framewidth", &fwidth );
						temp->Attribute( "frameheight", &fheight );
						temp->Attribute( "frames", &frames );
						temp->Attribute( "width", &twidth );
						temp->Attribute( "height", &theight );
						temp->Attribute( "fps", &anim->FPS );

						anim->FrameWidth = fwidth;
						anim->FrameHeight = fheight;
						anim->NumFrames = frames;
						anim->CurFrame = 0;
						anim->LastFrameTime = 0;

						sf::Image* imgfile = (sf::Image*)LoadImage( animfile );

						if( !imgfile )
						{
							std::cout << "Couldnt load sprite sheet: " << animfile << "(" << spr->SpriteName << ")" << std::endl;
							return NULL;
						}

						int i = 0, x = 0, y = 0;

						sf::Texture* tex;
						sf::Rect<int> bounds;

						while( i < frames )
						{
							if( x * fwidth == twidth )
							{
								x = 0;
								y++;
							}

							tex = new sf::Texture;

							bounds.top = y * fheight;
							bounds.left = x * fwidth;
							bounds.width = fwidth;
							bounds.height = fheight;

							tex->loadFromImage( *imgfile, bounds );

							anim->Frames.push_back( tex );

							x++;
							i++;
						}

						spr->State = Common::SPRITE_PLAY;
						spr->Animations[animname] = anim;
						spr->CurAnim = anim;
					}
				}
			}

			return spr;
		}
	}
}