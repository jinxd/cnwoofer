#ifndef SOUND_H
#define SOUND_H

#include <SFML/Audio.hpp>

#include <list>

namespace CNWoofer
{
	namespace Engine
	{
		class Sound
		{
			std::list<sf::Sound*> mSounds;

		public:

			Sound( void );
			~Sound( void );

			void PlaySound( sf::SoundBuffer* buffer );
			void Update( void );
			void SetVolume( float volume );

		};
	}
}

#endif 